FROM node:12.1 AS build
WORKDIR /app
COPY package.json .
COPY package-lock.json .

RUN npm install

COPY src src
COPY public public
RUN node ./node_modules/react-scripts/bin/react-scripts.js build


FROM nginx:alpine

COPY --from=build /app/build/* /usr/share/nginx/html/
COPY --from=build /app/build/* /usr/share/nginx/html/static/
COPY deploy/entrypoint.sh /
COPY deploy/default.conf /etc/nginx/conf.d/default.conf
RUN chmod +x /entrypoint.sh

ENV API_URL "https://api-adhesion.asso-insa-lyon.fr"
ENV SSO_AUTH_URL "https://sso.asso-insa-lyon.fr/auth/"
ENV CLIENT_ID "adhesion-public-frontend"
ENV REALM "asso-insa-lyon"

CMD ["/entrypoint.sh"]
