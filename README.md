# Frontend Adhésion grand public
Cette appli React sert à l'inscription des biz **1A** à la *VA* et au *WEI*.

Elle permettra aussi aux **2A+** de renouveler leur adhésion
et corriger leurs informations personnelles.

## Setup de dev
Lancer le backend (*adhésion-api* ) et entrer dans un terminal 
```shell script
(venv) user@hostname:~/pwd/api/$ ./manage.py migrate && ./manage.py seed-db
```
La commande `seed-db` insère des données de test et un compte de dev

Lancer le frontend React, et dans la console (javascript) de Firefox/Chrome, entrer ceci:
```javascript
window.localStorage.setItem('basictoken',window.btoa('selenium:hjklhjkl'))
```
Ça permet de s'authentifier sans keycloak sur le comte de test (*selenium* **/** *hjklhjkl*)

## Fonctionnement
### Authentification
Ce front React utilise Keycloak pour obtenir un token Bearer pour authentifier l'utilisateur sur les APIs REST.

Il communique avec adhésion-api avec ce token. Adhésion-api valide ce token auprès de Keycloak.

### Paiement
Le serveur adhésion-api intègrera sûrement une partie Mercanet ou alors ça sera sur un autre serveur (billetterie v2)

L'utilisateur sera redirigé 2-3 fois vers la page de paiement MercaNET.

## Design
Basé au maximum sur le travail d'Éloi & Arthur sur adobe Xd [lien](https://xd.adobe.com/view/073d38c2-3fad-4ddf-68a8-f2263c245ac5-710c/)

#
TODO
ne plus utiliser les classes avec componentIdidmount mais des HOOKS avec useEffect

useStyle pour le CSS ?

useState


## +
quand on choisit le WEI y'a plus de retour en arrière
mettre "adhérent" au lien de inscrit payé
ne pas forcer la mise en ligne de l'autorisation parentale (ne pas bloquer)

rajouter 'je m'engage à envoyer ma décharge parenter
 checkbox pour les mineurs
