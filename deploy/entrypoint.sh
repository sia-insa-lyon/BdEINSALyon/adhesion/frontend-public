#!/bin/sh -e
echo "Configuring UI to use API Server: ${API_URL}"
sed -i -e 's#__API_URL__#'"$API_URL"'#g' /usr/share/nginx/html/static/js/*.js
sed -i -e 's#__SSO_AUTH_URL__#'"$SSO_AUTH_URL"'#g' /usr/share/nginx/html/static/js/*.js
sed -i -e 's#__CLIENT_ID__#'"$CLIENT_ID"'#g' /usr/share/nginx/html/static/js/*.js
sed -i -e 's#__REALM__#'"$REALM"'#g' /usr/share/nginx/html/static/js/*.js
echo "compressing files"
gzip -vk9 /usr/share/nginx/html/static/js/*

echo "Starting Web Server"
nginx -g 'daemon off;'
