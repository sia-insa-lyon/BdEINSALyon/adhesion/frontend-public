const pdflib = require('pdf-lib')
const fs = require('fs');

const color = pdflib.rgb(0, 0, 0.4);

fs.readFile('public/formulaire-inscription.pdf', (err, data) => {
    pdflib.PDFDocument.load(data).then(pdfdoc => {
        const page = pdfdoc.getPage(0);
        const {width, height} = page.getSize()
        page.drawText('Y', { // homme
            x: width * (568 / 827),
            y: height * (1 - (616 / 1170)),
            size: 20,
            color: color
        })
        page.drawText('X', { // femme
            x: width * (647 / 827),
            y: height * (1 - (616 / 1170)),
            size: 20,
            color: color
        })
        page.drawText('Jean', { // prénom nom
            x: width * (578 / 827),
            y: height * (1 - (637 / 1170)),
            size: 12,
            color: color
        })
        page.drawText('22/01/1998', { // date de naissance
            x: width * (655 / 827),
            y: height * (1 - (655 / 1170)),
            size: 12,
            color: color
        })
        page.drawText('07 78 17 75 38', { // tel
            x: width * (610 / 827),
            y: height * (1 - (690 / 1170)),
            size: 12,
            color: color
        })
        page.drawText('jean.ribes@insa-lyon.fr', { // email
            x: width * (570 / 827),
            y: height * (1 - (708 / 1170)),
            size: 12,
            color: color
        })

        page.drawText('X', { // va
            x: width * (92 / 827),
            y: height * (1 - (702 / 1160)),
            size: 20,
            color: color
        })
        page.drawText('X', { // va+wei
            x: width * (92 / 827),
            y: height * (1 - (735 / 1160)),
            size: 20,
            color: color
        })

        pdfdoc.save().then((data) => { // promise hell, merci /sfrenot
            fs.writeFile('modif.pdf', data, console.log)
        }, err => console.log)
    })
})
