# Generated by Selenium IDE
# import pytest
import unittest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class SeleniumJ(unittest.TestCase):
    """
    Cela va lancer un navigateur !
    il faut installer Firefox et geckodrive (on peut faire avec CHrome et son driver aussi)
    """

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.vars = {}

    def tearDown(self):
        pass
        # self.driver.quit()

    def test_pay(self):
        self.driver.get("http://localhost:3000/")
        self.driver.execute_script("window.localStorage.setItem('basictoken',window.btoa('selenium:hjklhjkl'))")
        self.driver.add_cookie({
            "name": "useBasicAuth",
            "value": "true"
        })
        self.driver.find_element_by_id("loginButton").click()

        self.driver.find_element_by_id("completeMember").click()

        self.driver.find_element_by_id("memberBirthday").click()
        birthayInput = self.driver.find_element_by_id("memberBirthday")
        birthayInput.send_keys("30", Keys.TAB)
        birthayInput.send_keys("12", Keys.TAB)
        birthayInput.send_keys("2001")

        self.driver.find_element(By.ID, "memberBirthday").click()
        self.driver.find_element(By.ID, "memberBirthday").send_keys("2001-12-31")
        self.driver.find_element(By.ID, "studentNumber").click()
        self.driver.find_element(By.ID, "studentNumber").send_keys("4564654654")

        self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click() # clique sur "enregistrer"
