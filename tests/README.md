# test end-to-end

Des tests avec un navigateur automatisé ! C'est trop stylé.
Par contre il faut bien faire gaffe à paramétrer le réseau (le browser doit avoir accès au front & au back) et les applis
(les variables DOMAINS et HOST pour que Django accepte les connexion sur son addresse et depuis l'URL du front)
## Setup serveur Django
```shell script
rm db.sqlite3 && ./manage.py migrate && ./manage.py seed-db && ./manager.py runserver 0.0.0.0:8000
```

## Setup browser
le `-debug` et le port *5900* permettent un accès VNC avec le mot de passe 'secret'
```shell script
docker run --rm -d -p 4444:4444 -p 5900:5900 --name selenium  -v /dev/shm:/dev/shm selenium/standalone-chrome-debug
```

## Lancement des test
Normalement il n'y a pas besoin de driver spécial pour le RemoteDriver

```shell script
pip install selenium
python -m unittest selenium-re2e.py
```
