// note: NE PAS IMPORTER processs
export let apiUrl = process.env.NODE_ENV === 'production' ? (process.env.REACT_APP_IS_GITLAB_PAGES === 'true' ? 'https://api-adhesion.asso-insa-lyon.fr' : '__API_URL__') : 'http://localhost:8000';
export let authUrl = process.env.NODE_ENV === 'production' ? (process.env.REACT_APP_IS_GITLAB_PAGES === 'true' ? 'https://sso.asso-insa-lyon.fr/auth/' : '__SSO_AUTH_URL__') : 'http://localhost:8080/auth/';//'https://sso.asso-insa-lyon.fr/auth/';
export let clientId = process.env.NODE_ENV === 'production' ? (process.env.REACT_APP_IS_GITLAB_PAGES === 'true' ? 'adhesion-public-frontend' : '__CLIENT_ID__') : 'adhesion-public-frontend';
export let realm = process.env.NODE_ENV === 'production' ? (process.env.REACT_APP_IS_GITLAB_PAGES === 'true' ? 'asso-insa-lyon' : '__REALM__') : 'asso-insa-lyon';

export const bizuthDefaultDepartmentName = 'FIMI'; //le départ choisi par défaut pour les étudiants
export const bizuthDefaultYearshort_name = '1A'; //l'année choisie par défaut
export const bizuthDefaultSchoolName = 'INSA';
